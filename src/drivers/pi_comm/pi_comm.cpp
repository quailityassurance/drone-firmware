/*******************************************************
* Author: Quaility Assurance
* Date: 5/16/16
* Purpose: Receive Rasberry Pi signal and transmit GPS 
*	information from pixhawk to the Pi
*******************************************************/

#include <nuttx/clock.h>
#include <nuttx/arch.h>
#include <fcntl.h>

#include <sys/types.h>
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <poll.h>
#include <errno.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <px4_config.h>
#include <arc/board/board.h>

#include <drivers/drv_hrt.h>
#include <drivers/device/i2c.h>
#include <drivers/drv_gps.h>
#include <drivers/drv_pi.h>

#include <systemlib/systemlib.h>
#include <systemlib/perf_counter.h>
#include <systemlib/scheduling_priorities.h>
#include <systemlib/err.h>

#include <uORB/uORB.h>
#include <uORB/topics/vehicle_gps_position.h>
#include <uORB/topics/satellite_info.h>

#include <board_config.h>

#ifndef PI_DEFAULT_UART_PORT
#define PI_DEFAULT_UART_PORT "/dev/ttyS6"
#endif

#ifdef ERROR
# undef ERROR
#endif
static const int ERROR = -1;

class PI
{
public:
	PI(const char *uart_path);
	virtual ~PI();

	virtual int			init();

	void				print_info();

private:

	bool				task_should_exit;	//for PI class destructor
	int				_serial_fd;		//for setting up serial comm
	unsigned			_baudrate;		//baudrate
	char				_port[20];		//for displaying port in print_info...if we add
	volatile int			_task; //volatile int???
	bool				_healthy;		//not a bad pi or connection
	bool				_baudrate_change;	//haven't implemented, might be nice
	uint8_t				_should_send_message;	//value that is changed if read from pi tells to report command
	int				_gps_sub;		//deals with copying of outside struct from uORB application
	struct vehicle_gps_position_s	_gps_to_send;		//initial struct for copying gps data to this program
	orb_advert_t			_gps_to_send_pub; /* We shouldn't need because we're not posting a new report,
							    just copying from an existing */
	int				_temp_sub;		//deals with copying outside struct from uORB application
	struct temperature_s		_temp_to_send;		//initial struct for copying temperature data to this program
	orb_advert_t			_temp_to_send_pub; /* same as gps orb */
	struct pi_message		_message_to_send	//struct for sending required information
	float				_rate;			//not implemented
	//bool				_fake_pi;		//not implemented

	/**
	* Trampoline to the worker task
	*/
	static void			task_main_trampoline(void *arg);

	/**
	* Worker task: copies info from struct into sendable message
	*/
	void				task_main(void);

	/**
	* set the baudrate of the uart to the pi
	*/
	int				set_baudrate(unsigned baud);

	/**
	* receives the command to send information to the pi
	*/
	uint8_t				should_send();

	/**
	* attempt at printing base information that is being sent
	* just to see if information is actually making it to this
	* program
	*/
	void 				print_info();
};

/*
 * Driver 'main' command
 */
extern "C" __EXPORT int pi_main(int argc, char *argv[]);

namespace
{

PI	*g_dev = nullptr;

}

PI::PI(const char *uart_path) :
	task_should_exit(false),
	_healthy(false),
	_should_send_message(0),
	_gps_to_send(nullptr),
	_gps_to_send_pub(nullptr),
	_temp_to_send(nullptr),
	_temp_to_send_pub(nullptr),
	_rate(0.0f)
	//_fake_pi(fake_pi)
{
	/* store port name */
	strncpy(_port, uart_path, sizeof(_port));
	/* enforce null termination */
	_port[sizeof(_port) - 1] = '\0';
	/* potential need before it could be set in task_main */
	g_dev = this;
	memset(&_gps_to_send, 0, sizeof(_gps_to_send));
	memset(&_temp_to_send, 0, sizeof(_temp_to_send));
}

PI::~PI()
{
	/* we want the task to stop */
	_task_should_exit = true;

	/* spin waiting for the task to stop */
	for (unsigned i = 0; (i < 10) && (_task != -1); i++) {
		/*give a bit more time, 100ms */
		usleep(100000);
	}
	/* kill anyways if fail, will probably crash */
	if (_task != -1) {
		px4_task_delete(_task);
	}
	g_dev = nullptr;
}

int PI::init()
{
	/* start pi communication */
	_task = px4_task_spawn_cmd("pi", SCHED_DEFAULT,
				   SCHED_PRIORITY_SLOW_DRIVER, 1200, 
				   (px4_main_t)&PI::task_main_trampoline,
				    nullptr);
	if(_task < 0) {
		PX4_WARN("task start failed: %d", errno);
		return -errno;
	}
	return OK;
}

/* call to the main task*/
void PI::task_main_trampoline(void *arg)
{
	g_dev->task_main();
}

void PI::task_main()
{
	/* open the serial port */
	_serial_fd = ::open(_port, O_RDWR);

	if (_serial_fd < 0) {
		while (true) {
			PX4_WARN("failed to open serial port: %s err: %d", _port, errno);
		}
	_task = -1;
	exit(1);
	}

//not completely sure, deals with io of serial line though
#ifndef __PX4_QURT
	int flags = fcntl(_serial_fd, F_GETFL, 0);
	fcntl(_serial_fd, F_SETFL, flags | O_NONBLOCK);
#endif

	uint64_t last_rate_measurement = hrt_absolute_time(); //not truly using in this instance, still nice to have
	unsigned last_rate_count = 0;
	struct vehicle_gps_position_s gps_buf;	//temporary data structure
	struct temperature_s temp_buf;		//temporary data structure
	bool temp_updated;			//if data in temperature struct has changed
	bool gps_updated;			//if data in gps struct has changed

	_gps_sub = orb_subscribe(ORB_ID(vehicle_gps_position)); //get access to gps information
	_temp_sub = orb_subscribe(ORB_ID(temperature));		//get access to temperature information

	while(!task_should_exit)	//main task keeps checking for signal from pi, unless destructor is called
	{
		if(_should_send_message != 0)
		{
		orb_check(_gps_sub, &gps_updated);	//checks if gps data has changed
		orb_copy(ORB_ID(vehicle_gps_position), _gps_sub, &gps_buf);	//copies gps from outside into a local gps struct
		_gps_to_send.lat  = gps_buf.lat;	//temporary struct to local struct
		_gps_to_send.lon  = gps_buf.lon;	//lat and lon data copied

		orb_check(_temp_sub, &temp_updated);	//checks if temperature data has changed, assumes temperature driver is working
		orb_copy(ORB_ID(temperature), _temp_sub, &temp_buf); //copies temperature from outside into local temperature struct

		_temp_to_send.temperature_fahrenheit = temp_buf.temperature_fahrenheit;	//temporary to local struct
		_temp_to_send.temperature_celsius = temp_buf.temperature_celsius;

		_message_to_send.lat = _gps_to_send.lat;	//copies all recorded information into struct that will be sent back to the pi
		_message_to_send.lon = _gps_to_send.lon;
		_message_to_send.temperature_fahrenheit = _temp_to_send.temperature_fahrenheit;
		_message_to_send.temperature_celsius = _temp_to_send.temperature_celsius;

		write(_serial_fd, (const void *)&_message_to_send, sizeof(_message_to_send)); //sends the struct
		}	
	}
	PX4_WARN("exiting");
	::close(_serial_fd); //closes serial communication
	
	/* tell the dtor that we are exiting */
	_task = -1;
 	px4_task_exit(0);
}

/* read serial communication from pi and determines if information needs to be sent back */
uint8_t PI::should_send()
{
	uint8_t buf;
	::read(_serial_fd, buf, sizeof(buf));
	if( buf != 0)
	{
		_should_send_message = 1;
	}
}

/* for testing purposes, will cause main_task to copy information so it can be seen in print_info() */
uint8_t PI::test_send()
{
	_should_send_message = 1;
}

//prints info
void PI::print_info()
{
	PX4_WARN("lat: %d ", _message_to_send.lat );
	PX4_WARN("lon: %d", _message_to_send.lon );
	PX4_WARN("temperature(fahrenheit): %.2f ", (double)_message_to_send.temperature_fahrenheit );
	PX4_WARN("temperature(celsius): %.2f ", (double)_message_to_send.temperature_celsius);
}

/* Support shell commands */

namespace pi
{
PI	*g_dev = nullptr;

void 	start(const char *uart_path);
void	stop();
void	test();
void	reset();
void	info();

/* start the code for reading pi */
void start(const char *uart_path)
{
	if(g_dev != nullptr) {
		errx(1, "already started");
	}

	g_dev = new PI(uart_path);
	if(g_dev == nullptr) {
		goto fail;
	}

	if(g_dev != g_dev->init()) {
		goto fail;
	}

	return;

fail:

	if(g_dev != nullptr) {
		delete g_dev;
		g_dev = nullptr;
	}

	PX4_ERR("start failed");
	return;
}

void stop()
{
	delete g_dev;
	g_dev = nullptr;

	px4_task_exit(0);
}

void test()
{
	errx(0, "pass");
}

void reset()
{
	PX4_ERR("no reseting the pi from pixhawk");
	return;
}

void info()
{
	if( g_dev == nullptr) {
		errx(1, "not running");
	}
	g_dev -> print_info();

	return;
}

}

int pi_main(int argc, char *argv[])
{

 	const char *device_name = PI_DEFAULT_UART_PORT; 

	if(!strcomp(argv[1], "start"))
	{
		pi::start(device_name);
	}

	if(!strcomp(argv[1], "stop"))
        {
		pi::stop();
        }

        if(!strcomp(argv[1], "test"))
        {
		pi::test();
        }

        if(!strcomp(argv[1], "reset"))
        {
		pi::reset();
        }

        if(!strcomp(argv[1], "status"))
        {
		pi::info();
        }

	return 0;

out:
	PX4_ERR("unrecognized command, try 'start', 'stop', 'test', 
		 'reset', or 'status'");
	return 1;
}


