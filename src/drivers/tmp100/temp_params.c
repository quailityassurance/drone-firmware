/*********************************************************************
*Authors: Quaility Assurance Team				     *
*Purpose: Create a Parameter for a temperature sensor                *
*Date: 	  3/24/16						     *	
*********************************************************************/

#include <px4_config.h>

#include <systemlib/param/param.h>

/**
 *Temperature
 *
 *variable to keep track of temp from mounted sensor
 *
 *
 *@min -55.0
 *@max 125.0
 *unit celcius
 *@group Sensor
 */
PARAM_DEFINE_FLOAT(SENS_TEMP, 0.0f);	
