/*
* File: 	drv_tmp100.h
* Author: 	quaility assurance 
* Purpose: 	serve as a file for struct declaration and declare 
*		within uORB application
* Project: 	Senior Design
*/

#ifndef TMP100_H
#define TMP100_H

#include <drivers/drv_orb_dev.h>
#include <uORB/uORB.h>
#include <stdint.h>
#include <drivers/drv_sensor.h>
#include <sys/ioctl.h>


struct temperature_s {
	uint64_t	timestamp;		//microseconds since system boot
	float		temperature_fahrenheit;	//temperature
	float 		temperature_celsius;	//temperature in celsuis for them Europeans
};

ORB_DECLARE(temperature);

#endif	
