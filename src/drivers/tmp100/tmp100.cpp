/* 
* @file tmp100.cpp
* Name: tmp100.cpp
* Author : Quailitiy Assurance
* Purpose: run the tmp100 toy0045 temperature sensor on the pixhawk system
* date: 4-4-16
*/

#include <px4_config.h>

#include <sys/types.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <unistd.h>
#include <math.h>
#include <poll.h>
#include <errno.h>
#include <semaphore.h>

#include <nuttx/arch.h>
#include <nuttx/wqueue.h>
#include <nuttx/clock.h>

#include <systemlib/perf_counter.h>
#include <systemlib/err.h>
#include <systemlib/systemlib.h>

#include <drivers/device/ringbuffer.h>
#include <drivers/device/i2c.h>
#include <drivers/drv_hrt.h>
/* structure declaration for uORB advertising values */
#include <drivers/drv_tmp100.h>
#include <uORB/uORB.h>
#include "tmp100.h"

#include <board_config.h>

/* Some Configuration Constants */
#define TMP100_BUS		PX4_I2C_BUS_EXPANSION
#define TMP100_ADDR		0x4F //i2c address for temp sensor
#define REGISTER		0x01 //temp sensor register address
#define MEASURE_REGISTER	0x00 //temp sensor register address for	temp taking 	
#define TEST_REGISTER		0x02
#define TMP100_PATH		"/dev/tmp100"

#define RESOLUTIONBITS		0x20 //precision of temperature measure
#define CONVERSION_INTERVAL	(80000) /* microseconds */

#ifdef ERROR
# undef ERROR
#endif
static const int ERROR =-1;

class TMP100 : public device::I2C
{
public:
	TMP100(int bus = TMP100_BUS, int address = TMP100_ADDR, unsigned int conversion_interval = CONVERSION_INTERVAL, const char *path = TEMPERATURE_SENSOR_BASE_DEVICE_PATH);
	virtual ~TMP100();

	virtual int	init(); //i2c command to start i2c bus

	virtual ssize_t read(struct file *filep, char *buffer, size_t buflen); 
	virtual int	ioctl(struct file *filp, int cmd, unsigned long arg);	

	void 		print_info(); //print diagnostic info about driver

protected:
	virtual int	probe();

private:

	work_s				_work;
	ringbuffer::RingBuffer		*_reports;
	bool				_sensor_ok;
	uint8_t				_valid;
	int				_measure_ticks;
	bool				_collect_phase;
	int				_class_instance;
	int				_orb_class_instance;
	unsigned			_conversion_interval;

	orb_advert_t			_temperature_sensor_topic;

	perf_counter_t			_sample_perf;
	perf_counter_t			_comms_errors;
	perf_counter_t			_buffer_overflows;
	
	/*
	* Test to see if sensor is present at the specified address
	*/
	int 		probe_address(uint8_t address);
	/* start automatic meausrements */
	void		start();
	
	/* stop auto measurents */
	void		stop();

	/*static trampoline from wqueue context
	*@param arg	instance pointer for the driver that is polling
	*/
	static void cycle_trampoline(void *arg);
		
	/*perform a poll cycle; collect from the previous measurement
	* and start a new one
	*/
	void		cycle();
	int		measure(); //getTemperature rewrite
	int		collect();
	
	//set precision of temperature readings, e.g.: 10.1 or 10.12 
	int 		setResolution();

};
 
/*
 * Driver 'main' command.
 */
extern "C" __EXPORT int tmp100_main(int argc, char *argv[]);

TMP100::TMP100(int bus, int address, unsigned int conversion_interval, const char *path):
	I2C("TMP100", path, bus, address, 100000),
	_reports(nullptr),
	_sensor_ok(false),
	_valid(0),
	_measure_ticks(0),
	_collect_phase(false),
	_class_instance(-1),
	_conversion_interval(conversion_interval),
	_temperature_sensor_topic(nullptr),
	_sample_perf(perf_alloc(PC_ELAPSED, "tmp100_read")),
	_comms_errors(perf_alloc(PC_COUNT, "tmp100_comms_errors")),
	_buffer_overflows(perf_alloc(PC_COUNT, "tmp_buffer_overflow"))
	{
	//retries in case device misses first measure
	I2C::_retries = 3;
	//enable debug() calls
	_debug_enabled = false;
	//work_cancel in the dtor will explode if this isn't done
	memset(&_work, 0, sizeof(_work));
	}

TMP100::~TMP100()
{
	stop();

	/* free any existing reports */
	if(_reports != nullptr){
		delete _reports;
	}
	
	/* ensure the inactive state */
	if(_class_instance != -1) {
		unregister_class_devname(TEMPERATURE_SENSOR_BASE_DEVICE_PATH, _class_instance);
	}
	
	//free perf counter
	perf_free(_sample_perf);
	perf_free(_comms_errors);
	perf_free(_buffer_overflows);
}

//Initialize sensor connection via i2c bus
int TMP100::init()
{
	//everything returns ret, set for worst case first
	int ret = ERROR;
	//do i2c init (and probe) first
	if (I2C::init() != OK) {
		goto out;
	}
	//allocate buffer storing values to be read from temp sensor
	_reports = new ringbuffer::RingBuffer(3, sizeof (struct temperature_s));

	if(_reports == nullptr) {
		goto out;
	}
	
	_class_instance = register_class_devname(TEMPERATURE_SENSOR_BASE_DEVICE_PATH);
	
	
	if(_class_instance == CLASS_DEVICE_PRIMARY){
		struct temperature_s ts_report;
		measure();
		_reports->get(&ts_report);

		_temperature_sensor_topic = orb_advertise(ORB_ID(temperature), &ts_report);

		if(_temperature_sensor_topic == nullptr) {
		DEVICE_LOG("failed to create a temperature_sensor object. uORB started?");
		}
	}
	ret = OK;

	_sensor_ok = true;

out:
	return ret;
}


int TMP100::probe()
{
	_retries = 4;	
	int ret = measure();
	
	_retries = 2;
	return ret;
}

int TMP100::ioctl(struct file *filp, int cmd, unsigned long arg)
{
        switch (cmd) {

        case SENSORIOCSPOLLRATE: {
                        switch (arg) {

                        /* switching to manual polling */
                        case SENSOR_POLLRATE_MANUAL:
                                stop();
                                _measure_ticks = 0;
                                return OK;

                        /* external signalling (DRDY) not supported */
                        case SENSOR_POLLRATE_EXTERNAL:

                        /* zero would be bad */
                        case 0:
                                return -EINVAL;

                        /* set default/max polling rate */
                        case SENSOR_POLLRATE_MAX:
                        case SENSOR_POLLRATE_DEFAULT: {
                                        /* do we need to start internal polling? */
                                        bool want_start = (_measure_ticks == 0);

                                        /* set interval for next measurement to minimum legal value */
                                        _measure_ticks = USEC2TICK(_conversion_interval);

                                        /* if we need to start the poll state machine, do it */
                                        if (want_start) {
                                                start();
                                        }

                                        return OK;
                                }

                        /* adjust to a legal polling interval in Hz */
                        default: {
                                        /* do we need to start internal polling? */
                                        bool want_start = (_measure_ticks == 0);

                                        /* convert hz to tick interval via microseconds */
                                        unsigned ticks = USEC2TICK(1000000 / arg);

                                        /* check against maximum rate */
                                        if (ticks < USEC2TICK(_conversion_interval)) {
                                                return -EINVAL;
                                        }

                                        /* update interval for next measurement */
                                        _measure_ticks = ticks;

                                        /* if we need to start the poll state machine, do it */
                                        if (want_start) {
                                                start();
                                        }

                                        return OK;
                                }
                        }
                }

        case SENSORIOCGPOLLRATE:
                if (_measure_ticks == 0) {
                        return SENSOR_POLLRATE_MANUAL;
                }

                return (1000 / _measure_ticks);

        case SENSORIOCSQUEUEDEPTH: {
                        /* lower bound is mandatory, upper bound is a sanity check */
                        if ((arg < 1) || (arg > 100)) {
                                return -EINVAL;
                        }

                        irqstate_t flags = irqsave();

                        if (!_reports->resize(arg)) {
                                irqrestore(flags);
                                return -ENOMEM;
                        }

                        irqrestore(flags);

                        return OK;
                }

        case SENSORIOCGQUEUEDEPTH:
                return _reports->size();

        case SENSORIOCRESET:
                /* XXX implement this */
                return -EINVAL;

	default:
		/*give it to the superclass */
		return I2C::ioctl(filp, cmd, arg);
	}
}

ssize_t TMP100::read(struct file *filep, char *buffer, size_t buflen)
{
	unsigned count = buflen / sizeof(temperature_s);
	temperature_s *tbuf = reinterpret_cast<temperature_s *>(buffer);
	int ret = 0;
	
	//buffer must be large enough
	if (count < 1) {
		return -ENOSPC;
	}
	//if auto measure is enabled
	if (_measure_ticks > 0) {
		while (count--) {
			if(_reports->get(tbuf)) {
				ret += sizeof(*tbuf);
				tbuf++;
			}
		}
		//if no data, warn the caller
		return ret ? ret : -EAGAIN;
	}

	//manual measure
	do {
		_reports->flush();
	
		if (OK != measure()) {
			ret = -EIO;
			break;
		}
		//wait for complete
		usleep(_conversion_interval);
		//run the collectioin phase
		if (OK != collect()) {
			ret = -EIO;
			break;
		}
		/*state machine will have generated a report, copy it 
		* out
		*/
		if (_reports->get(tbuf)) {
			ret = sizeof(*tbuf);
		}
	} while (0);
	
	return ret;
}

//start reading from sensor, to mimic setup() from original
void TMP100::start()
{
	/* flush ring and reset state machine */
	_collect_phase = false;
	_reports->flush();

	/*start the work queue*/
	work_queue(HPWORK, &_work, (worker_t)&TMP100::cycle_trampoline, this, 1); //opens link on i2c bus
}

//stop reading from the sensor
void TMP100::stop()
{
	work_cancel(HPWORK, &_work);
}

int TMP100::measure()
{
	int ret;

	const uint8_t cmd = MEASURE_REGISTER;
	ret = transfer(&cmd, 1, nullptr, 0);
	
	if (OK != ret) {
		perf_count(_comms_errors);
		DEVICE_LOG("i2c::transfer returned %d", ret);
	}	

	return ret;
}

int TMP100::collect()
{
	int ret = -EIO;
	//read from sensor
	uint8_t val[2] = {0, 0};
	
	perf_begin(_sample_perf);
	
	ret = transfer(nullptr, 0, &val[0], 2);

	if (ret < 0) {
		DEVICE_LOG("error reading from sensor: %d", ret);
		perf_count(_comms_errors);
		perf_end(_sample_perf);
		return ret;
	}
	
	int tempSUM = ((val[0] << 8) | val[1]) >> 4;
	float celsius = tempSUM * 0.0625;
	float temp = (celsius * 1.8f) + 32;
	struct temperature_s report;

	report.timestamp = hrt_absolute_time();
	report.temperature_fahrenheit = temp;
	report.temperature_celsius = celsius;

	if(_temperature_sensor_topic != nullptr) {
		orb_publish(ORB_ID(temperature), _temperature_sensor_topic, &report);
	}
	if(_reports->force(&report)) {
		perf_count(_buffer_overflows);
	}
	poll_notify(POLLIN);
	ret = OK;
	perf_end(_sample_perf);
	return ret;
}

void TMP100::cycle_trampoline(void *arg)
{
	TMP100 *dev = (TMP100 *)arg;

	dev->cycle();
}

/* wrote out cycle code, but haven't really used it yet.*/
void TMP100::cycle()
{
	int ret;

	if(_collect_phase) {
		ret = collect();
		if(OK != ret) {
			perf_count(_comms_errors);
			start();
			_sensor_ok = false;
			return;
 		 }
		_collect_phase = false;

		if(_measure_ticks > USEC2TICK(CONVERSION_INTERVAL)) {
		
		//schedule next cyclework_queue
			work_queue(HPWORK, &_work, (worker_t)&TMP100::cycle_trampoline, this, USEC2TICK(_measure_ticks - CONVERSION_INTERVAL)); 

			return;
		}
	}
	
	ret = measure();

	if (OK != ret) {
		DEVICE_DEBUG("measure error");
	}

	//next phase is collection
	_sensor_ok = (ret == OK);

	_collect_phase = true;
	//schedule fresh cycle call when measure is done
	work_queue(HPWORK, &_work, (worker_t)&TMP100::cycle_trampoline, this, USEC2TICK(CONVERSION_INTERVAL));
}

void TMP100::print_info()
{
	perf_print_counter(_sample_perf);
	perf_print_counter(_comms_errors);
	perf_print_counter(_buffer_overflows);
	printf("poll interval: %u ticks\n", _measure_ticks);
	_reports->print_info("report queue");
}

/* precision of temperature reading, e.g.:10.1 or 10.12 or 10.123 */
int TMP100::setResolution()
{
 int ret;	//error checking
 const uint8_t resolution = RESOLUTIONBITS;	//sets resolution values
 const uint8_t reg = REGISTER;
 const uint8_t meas = MEASURE_REGISTER;

 ret = transfer(&reg, 1, nullptr, 0);	//mimic wire.h's wire.send, addresses register on sensor to set resolution
 ret = transfer(&resolution, 1, nullptr, 0);    //actuall sets resolution
 ret = transfer(&meas, 1, nullptr, 0);         //set register on sensor back to temperature reading
 return ret;
}


namespace tmp100
{

#ifdef ERROR
# undef ERROR
#endif

const int ERROR = -1;

TMP100 *g_dev = nullptr;	//global instance of TMP100 class

void start(int i2c_bus);	//starts sensor i2c init
void stop();				//stops
void test();				//next three are here because everything else had them, didn't implement
void reset();
void info();

void start(int i2c_bus)
{
	int fd;

	if (g_dev != nullptr) {
		errx(1, "already started");
	}
	
	//create the driver
	g_dev = new TMP100(i2c_bus);

	if(g_dev == nullptr) {
		goto fail;
	}

 	if( OK != g_dev->init()){
		goto fail;
	}

	fd = open(TEMPERATURE_SENSOR0_DEVICE_PATH, O_RDONLY);

	if (fd < 0) {
		goto fail;
	}
	if(ioctl(fd, SENSORIOCSPOLLRATE, SENSOR_POLLRATE_DEFAULT) < 0) {
		goto fail;
	}
	
	exit(0);
fail:
	if(g_dev != nullptr) {
		delete g_dev;
		g_dev = nullptr;
	}
	errx(1, "no tmp100 sensor connected");
}

void stop()
{
	if(g_dev != nullptr) {
		delete g_dev;
		g_dev = nullptr;
	}
	else {
		errx(1, "driver not running");
	}
	exit(0);
}

void test()
{
	struct temperature_s report;
	ssize_t sz;
	int ret;

	int fd = open(TMP100_PATH, O_RDONLY);

	if (fd < 0) {
		err(1, "%s open failed (try 'tmp100 start' if the driver is not running", TMP100_PATH);
	}

	sz = read(fd, &report, sizeof(report));
	if (sz != sizeof(report)) {
		err(1, "immediate read failure");
	}

	warnx("single read");
	warnx("measurement: %0.2f ", (double)report.temperature_fahrenheit);
	warnx("time: 	    %llu", report.timestamp);

	if( OK != ioctl(fd, SENSORIOCSPOLLRATE, 2)) {
		errx(1, "failed to set 2Hz poll rate");
	}
	//read sensor 50x and report each value
	for (unsigned i = 0; i < 50; i++) {
		struct pollfd fds;
	
		fds.fd = fd;
		fds.events = POLLIN;
		ret = poll(&fds, 1, 2000);
	
		if (ret != 1) {
			errx(1, "timed out waiting for sensor data");
		}
		sz = read(fd, &report, sizeof(report));

		if (sz != sizeof(report)) {
			err(1, "periodic read failed");
		}

		warnx("periodic read %u", i);
		warnx("measurement: %0.3f", (double)report.temperature_fahrenheit);
		warnx("time:	    %llu", report.timestamp);
	}

		//reset sensor polling to default
	if( OK != ioctl(fd, SENSORIOCSPOLLRATE, SENSOR_POLLRATE_DEFAULT)) {
			errx(1, "failed to set default poll rate");		}
		errx(0, "PASS");

}


void reset()
{
	int fd = open(TMP100_PATH, O_RDONLY);
	
	if (fd < 0) {
		err(1, "failed");
	}
	if (ioctl(fd, SENSORIOCRESET, 0) < 0) {
		err(1, "driver reset failed");
	}
	if (ioctl(fd, SENSORIOCSPOLLRATE, SENSOR_POLLRATE_DEFAULT) < 0) {	
		err(1, "driver poll restart failed");
	}
	exit(0);

}

void info()
{
	if(g_dev == nullptr) {
		errx(1,"not running");
	}
	
	printf("state @ %p\n", g_dev);
	g_dev->print_info();

	exit(0);
}

}//namespace

int tmp100_main(int argc, char *argv[])	//code for if someone tried to run from nsh(pixhawk shell) 
{
	int i2c_bus = TMP100_BUS;
	int i;

	for( i = 1; i < argc; i++) {
		if(strcmp(argv[i], "-b") == 0 || strcmp(argv[i], "--bus") == 0) {
			if(argc > i + 1) {
				i2c_bus = atoi(argv[i + 1]);
			}
		}
	}

	if(!strcmp(argv[1], "start")) {
		tmp100::start(i2c_bus);
	}
	if(!strcmp(argv[1], "stop")){
		tmp100::stop();
	}
	if(!strcmp(argv[1], "test")){
		tmp100::test();
	}
	if(!strcmp(argv[1], "reset")){
		tmp100::reset();
	}
	if(!strcmp(argv[1], "info") || !strcmp(argv[1], "status")){
		tmp100::info();
	}
	exit(0);
}     
