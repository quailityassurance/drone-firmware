/****************************************************************
* Author: 	Quaility Assurance
* Date:   	5/18/16
* Purpose:	Structure declaration for message to be sent via
*		serial line
****************************************************************/

#include <stdint.h>
#include <sys/ioctl.h>

#include "drv_sensor.h"
#include "drv_orb_dev.h"

struct pi_message {
	int32_t lat;
	int32_t lon;
	float	temperature_fahrenheit;
	float   temperature_celsius;
};
