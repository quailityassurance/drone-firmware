/*
* File: 	drv_tmp100.h
* Author: 	quaility assurance 
* Purpose: 	serve as a file for struct declaration and declare 
*		within uORB application
* Project: 	Senior Design
*/

#ifndef _DRV_TMP100_H
#define _DRV_TMP100_H

#include <stdint.h>
#include <sys/ioctl.h>
#include "drv_orb_dev.h"
#include "drv_sensor.h"

#define TEMPERATURE_SENSOR_BASE_DEVICE_PATH	"/dev/temperature_sensor"
#define TEMPERATURE_SENSOR0_DEVICE_PATH		"/dev/temperature_sensor0"

/*
 * ioctl() defs
 *
 *
 * Tmp100 drivers also implement the generic sensor driver interfaces
 * from drv_sensor.h
 */

 #define _TMP100IOCBASE		(0x7800)
 #define _TMP100IOC(_n)		(_PX4_IOC(_TMP100IOCBASE, _n))

 #define TMP100IOSSCALE		__TMP100IOC(0)
 #define TMP100IOGSCALE		__TMP100IOC(1)

#endif /* DRV_TMP100_H */	
